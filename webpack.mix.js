let mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.webpackConfig({
  resolve: {
    alias: {
      '@': __dirname + '/resources/js'
    },
  },
});

mix.js('resources/js/app/app.js', 'public/js')
.js('resources/js/admin/admin.js', 'public/js')
    .sass('resources/sass/marketing/marketing.scss', 'public/css')
    .sass('resources/sass/app/app.scss', 'public/css')
    .sass('resources/sass/admin/admin.scss', 'public/css')
    .options({
        processCssUrls: false, 
        postCss:[ 
            tailwindcss('./tailwind.config.js') 
        ],
        globalVueStyles: './resources/sass/_variables.scss',
        extractVueStyles: true,
    });


if (mix.inProduction()) {
    mix.version();
}