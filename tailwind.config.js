module.exports = {
    theme: {
      extend: {
        height: {
          '7': '1.75rem',
        },
        margin: {
          '7': '1.75rem',
        },
        width: {
          '7': '1.75rem',
        },
      },
    },
    variants: {
      // Some useful comment
    },
    plugins: [
      // Some useful comment
    ],
  };
  