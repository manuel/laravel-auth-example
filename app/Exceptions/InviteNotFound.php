<?php

namespace App\Exceptions;

use Exception;

class InviteNotFound extends Exception
{
    public function render($request)
    {
        return view('exceptions.invite-not-found');
    }
}
