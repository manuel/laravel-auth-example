<?php

namespace App\Libs;

use App\Exceptions\InvalidCredentialsException;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Cookie;

class LoginProxy
{
    const REFRESH_TOKEN = 'refreshToken';

    private $auth;

    private $response;

    private $db;

    private $request;

    private $user;

    public function __construct(Application $app, User $user)
    {
        $this->user = $user;

        $this->auth = $app->make('auth');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws InvalidCredentialsException
     */
    public function attemptLogin($email, $password)
    {
        $user = $this->user->where('email', '=', $email)->first();

        if (!is_null($user)) {
            $response = $this->proxy(
          'password',
          [
          'username' => $email,
          'password' => $password,
        ]
      );

            return $response;
        }

        throw new InvalidCredentialsException();
    }

    /**
     * Attempt to refresh the access token used a refresh token that
     * has been saved in a cookie
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws InvalidCredentialsException
     */
    public function attemptRefresh()
    {
        $refreshToken = $this->request->cookie(self::REFRESH_TOKEN);

        return $this->proxy(
        'refresh_token',
        [
        'refresh_token' => $refreshToken,
      ]
    );
    }

    /**
     * Proxy a request to the OAuth server.
     *
     * @param string $grantType what type of grant type should be proxied
     * @param array $data the data to send to the server
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws InvalidCredentialsException
     */
    public function proxy($grantType, array $data = [])
    {
        $data = array_merge(
        $data,
        [
        'client_id' => config('auth.clients.app_password_grant.id'),
        'client_secret' => config('auth.clients.app_password_grant.secret'),
        'grant_type' => $grantType,
      ]
    );

        $protocol = 'http';
        if ($this->request->secure()) {
            $protocol = 'https';
        }

        $response = $this->doPost("{$protocol}://{$this->request->getHost()}/oauth/token", $data);

        if (!$response) {
            throw new InvalidCredentialsException();
        }

        $data = json_decode($response->getBody());

        // Create a refresh token cookie
        return response()
      ->json(
          [
          'api_token' => $data->access_token,
          'expires_in' => $data->expires_in,
        ]
      );
    }

    /**
     * Logs out the user. We revoke access token and refresh token.
     * Also instruct the client to forget the refresh cookie.
     */
    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        $this->db
      ->table('oauth_refresh_tokens')
      ->where('access_token_id', $accessToken->id)
      ->update(
          [
          'revoked' => true,
        ]
      );

        $accessToken->revoke();

        return response(null, 204);
    }

    /**
     * @param string $url
     * @param array $data
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    private function doPost($url, $data)
    {
        try {
            $client = new Client();
            $response = $client->request(
          'POST',
          $url,
          [
          'form_params' => $data,
        ]
      );

            return $response;
        } catch (ClientException $e) {
        } catch (GuzzleException $e) {
        }
    }
}
