<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminWebRoutes();
         
        $this->mapAdminApiRoutes();

        $this->mapAppPassportApiRoutes();

        $this->mapAdminPassportApiRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::domain($this->baseDomain())
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/consumer/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::domain($this->baseDomain())
            ->prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/consumer/api.php'));
    }

    /**
     * Define the admin "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminWebRoutes()
    {
        Route::domain($this->baseDomain('admin'))
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/admin/web.php'));
    }

    /**
     * Define the admin "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminApiRoutes()
    {
        Route::domain($this->baseDomain('admin'))
            ->prefix('api')
            ->middleware('api')
            ->middleware('passport.admin')
            ->namespace($this->namespace)
            ->group(base_path('routes/admin/api.php'));
    }

    /**
     * Define the admin "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAppPassportApiRoutes()
    {
        Route::domain($this->baseDomain())
            ->group(base_path('routes/consumer/passport.php'));
    }

    /**
     * Define the admin "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminPassportApiRoutes()
    {
        Route::domain($this->baseDomain('admin'))
            ->middleware('passport.admin')
            ->group(base_path('routes/admin/passport.php'));
    }

    private function baseDomain(string $subdomain = ''): string
    {
        if (strlen($subdomain) > 0) {
            $subdomain = "{$subdomain}.";
        }

        return $subdomain . config('app.base_domain');
    }
}
