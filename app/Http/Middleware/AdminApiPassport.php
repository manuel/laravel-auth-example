<?php

namespace App\Http\Middleware;

use App\Models\AdminToken;
use Closure;
use Laravel\Passport\Passport;

class AdminApiPassport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        Passport::useTokenModel(AdminToken::class);
        
        return $next($request);
    }
}
