<?php

namespace App\Http\Controllers;

class HealthCheckController extends Controller
{
    public function check()
    {
        return response('', 200)
            ->header('Content-Type', 'text/plain');
    }
}
