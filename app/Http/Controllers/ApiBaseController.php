<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiBaseController extends Controller
{
    public function sanitizeInput(Request $request)
    {
        $inputs = $this->sanitizeArray($request->all());
        $request->replace($inputs);

        return $request;
    }

    public function getUser()
    {
        return Auth::user();
    }

    private function sanitizeArray(array $inputs)
    {
        foreach ($inputs as $index => $input) {
            if (is_array($inputs[$index])) {
                $inputs[$index] = $this->sanitizeArray($inputs[$index]);
            } else {
                $inputs[$index] = isset($inputs[$index]) && !is_bool($inputs[$index]) ? clean($input) : $inputs[$index];
            }
        }
        return $inputs;
    }
}
