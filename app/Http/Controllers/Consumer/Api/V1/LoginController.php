<?php

namespace App\Http\Controllers\Consumer\Api\V1;

use App\Http\Controllers\Controller;
use App\Libs\LoginProxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate(
          [
          'email' => 'required',
          'password' => 'required',
        ]
      );
        $email = $request->get('email');
        $password = $request->get('password');

        try {
            return $this->loginProxy->attemptLogin($email, $password);
        } catch (\Exception $e) {
            //@todo what to do when an exception occurs?
            Log::error($e->getMessage());
        }
    }

    public function refresh()
    {
        return $this->loginProxy->attemptRefresh();
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        return $this->loginProxy->logout();
    }
}
