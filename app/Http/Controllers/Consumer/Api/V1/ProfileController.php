<?php

namespace App\Http\Controllers\Consumer\Api\V1;

use App\Http\Controllers\ApiBaseController;

class ProfileController extends ApiBaseController
{
    public function index()
    {
        return $this->getUser();
    }
}
