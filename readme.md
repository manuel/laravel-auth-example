# Installation

The software uses Laravel as the main software.

Needed software:
1. [Laravel Install Instrutions](https://laravel.com/docs/5.8)
2. Apache/Nginx
3. Node
4. Redis
5. PostgresSQL

Once you have all the software installed follow these instructions:
1. run `composer install`
2. run `npm i`
3. run `cp .env.example .env`
4. run `php artisan  key:generate`
6. create a new database
6. edit `.env` to match the database you created in the previous step
7. run `php artisan migrate`
8. run `php artisan passport:keys`
9. run `php artisan passport:client --password` and name it 'authexample password grant client'. Take note of the client id and client secret and copy/paste them to the .env APP_PASSWORD_CLIENT_ID and APP_PASSWORD_CLIENT_SECRET keys
10. run `php artisan passport:client --personal` and use the default name. There's no need to write down this key
11. run `php artisan passport:client --password` and name it 'authexample admin password grant client'. Take note of the client id and client secret and copy/paste them to the .env ADMIN_PASSWORD_CLIENT_ID and ADMIN_PASSWORD_CLIENT_SECRET keys
12. add the following entries to your /etc/hosts file:
`
127.0.0.1       local.authexample.com
127.0.0.1       local.admin.authexample.com
`
13.  add the following vhost to your apache config:
```
<VirtualHost *:80>
    ServerName local.authexample.com
    ServerAlias admin.local.authexample.com

    DocumentRoot /path/to/authexample/public
    RewriteEngine On
    RewriteRule ^/s3/(.*) http://authexample-dev.s3.amazonaws.com/$1 [P]
    php_value upload_max_filesize 32M
    <Directory /path/to/authexample/public>
        Options FollowSymLinks
        Require all granted
        DirectoryIndex index.php

        RewriteEngine On

        # Redirect Trailing Slashes If Not A Folder...
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^(.*)/$ /$1 [L,R=301]


        # Handle Front Controller...
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ index.php [L]

        # Handle Authorization Header
        RewriteCond %{HTTP:Authorization} .
        RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
    </Directory>
</VirtualHost>
```
14. restart apache
15. run `php artisan db:seed --class=AdminUserSeeder`
16. go to http://local.admin.authexample.com and login
