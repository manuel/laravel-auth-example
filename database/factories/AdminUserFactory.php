<?php

use App\Models\AdminUser;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(AdminUser::class, function (Faker $faker, array $attrib) {
    $password = (array_key_exists('password', $attrib)) ? $attrib['password'] : Hash::make('secretonething');

    return [
        'id' => Uuid::uuid4()->toString(),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password,
        'remember_token' => Str::random(10),
    ];
});
