<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Un Guard model
        Model::unguard();

        $firstName = $this->command->ask('What\'s your first name?');
        $lastName = $this->command->ask('What\'s your last name?');
        $email = $this->command->ask('What\'s your email?');
        $password = $this->command->ask('What\'s your password?');

        factory(App\Models\AdminUser::class)->create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        // Re Guard model
        Model::reguard();
        $this->command->info('User Created!');
    }
}
