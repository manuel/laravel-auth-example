<?php

namespace Tests\Http\Api;

use Faker\Factory as FakerFactory;
use Tests\TestCase;
use Tests\Traits\TestAdminUserSetupTrait;

/**
 * Base class for API test classes
 *
 * Class BaseTest
 * @package Tests\Feature
 */
abstract class BaseTest extends TestCase
{
    use TestAdminUserSetupTrait;

    const PASSWORD = 'secret1';

    protected $resource = null;
    protected $resource_url = 'admin.local.peersway.com/api/v1/';
    protected $faker = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->createAdminUser(self::PASSWORD);

        $this->actingAs($this->user, 'api');

        $this->faker = FakerFactory::create();
    }
}
