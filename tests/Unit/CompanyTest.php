<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Company;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    /**
     * @test
     * A company can be created
     *
     * @return void
     */
    public function it_can_create_a_company()
    {
        $address = factory(Address::class)->create();
        $data = [
            'name' => 'Test Company',
            'email' => 'test@email.com',
            'phone' => '1234567890',
            'address_id' => $address->id,
        ];

        $company = Company::create($data);
        $this->assertInstanceOf(Company::class, $company);
        $this->assertEquals($data['name'], $company->name);
        $this->assertEquals($data['email'], $company->email);
        $this->assertEquals($data['name'], $company->name);
        $this->assertEquals($data['address_id'], $address->id);
    }

    /**
     * @test
     * Creating a Company without a name
     *
     * @return void
     */
    public function a_company_should_have_a_name()
    {
        try {
            $company = Company::create([
                'name' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without a name, but an exception was not thrown.');
    }

    /**
     * @test
     * Creating a Company without an email
     *
     * @return void
     */
    public function a_company_should_have_an_email()
    {
        try {
            $company = Company::create([
                'email' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without a email, but an exception was not thrown.');
    }
    
    /**
     * @test
     * Creating a Company without an phone
     *
     * @return void
     */
    public function a_company_should_have_a_phone()
    { 
        try {
            $company = Company::create([
                'phone' => null
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without a phone, but an exception was not thrown.');
    }

    /**
     * @test
     * A company should have a name
     *
     * @return void
     */
    public function company_should_have_name()
    {
        try {
            $company = Company::create([
                'name' => null
            ]);


        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without a name, but an exception was not thrown.');
    }

    /**
     * @test
     * A company should have an email
     *
     * @return void
     */
    public function company_should_have_email()
    {
        
        try {
            $company = Company::create([
                'email' => null
            ]);

        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without a email, but an exception was not thrown.');
    }
    
    /**
     * @test
     * A company should have an phone
     *
     * @return void
     */
    public function company_should_have_phone()
    {
        
        try {
            $company = Company::create([
                'phone' => null
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without a phone, but an exception was not thrown.');
    }

    /**
     * @test
     * A company should have an address
     *
     * @return void
     */
    public function a_company_should_have_an_address()
    {
        try {
            $company = Company::create([
                'address_id' => null
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a company without an address, but an exception was not thrown.');
    }
}
