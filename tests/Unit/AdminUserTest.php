<?php

namespace Tests\Unit;

use App\Models\AdminUser;
use Tests\TestCase;

class AdminUserTest extends TestCase
{
    /**
     * @test
     * An Admin User can be created
     *
     * @return void
     */
    public function it_can_create_an_admin_user()
    {
        $data = [
            'first_name' => 'Test',
            'last_name' => 'User',
            'email' => 'email@email.com',
            'password' => 'asdadasdsad',
        ];

        $adminUser = new AdminUser($data);
        $adminUser->save();    
        $this->assertInstanceOf(AdminUser::class, $adminUser);
        $this->assertEquals($data['first_name'], $adminUser->first_name);
        $this->assertEquals($data['last_name'], $adminUser->last_name);
        $this->assertEquals($data['email'], $adminUser->email);
        $this->assertEquals($data['password'], $adminUser->password);
    }
}
