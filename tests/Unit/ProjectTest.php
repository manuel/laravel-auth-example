<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Models\Company;
use App\Models\Project;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    /**
     * @test
     * A Project can be created
     *
     * @return void
     */
    public function it_can_create_a_project()
    {
        $company = factory(Company::class)->create();
        $address = factory(Address::class)->create();
        $data = [
            'name' => 'Test Project',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'test@test.com',
            'address_id' => $address->id,
        ];

        $project = new Project($data);
        $company->projects()->save($project);
        $this->assertInstanceOf(Project::class, $project);
        $this->assertEquals($data['name'], $project->name);
        $this->assertEquals($data['first_name'], $project->first_name);
        $this->assertEquals($data['last_name'], $project->last_name);
        $this->assertEquals($data['email'], $project->email);
        $this->assertEquals($data['address_id'], $address->id);
    }

    /**
     * @test
     * Creating a Project without a Company
     *
     * @return void
     */
    public function a_project_has_to_have_a_company_id()
    {
        try {
            $project = factory(Project::class)->create([
                'company_id' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a project without a company, but an exception was not thrown.');
    }
    
    /**
     * @test
     * Creating a Project without a name
     *
     * @return void
     */
    public function a_project_has_to_have_a_name()
    {
        try {
            $project = factory(Project::class)->create([
                'name' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a project without a name, but an exception was not thrown.');
    }
    
    /**
     * @test
     * Creating a Project without a first name
     *
     * @return void
     */
    public function a_project_has_to_have_a_first_name()
    {
        try {
            $project = factory(Project::class)->create([
                'first_name' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a project without a first name, but an exception was not thrown.');
    }

    /**
     * @test
     * Creating a Project without a last name
     *
     * @return void
     */
    public function a_project_has_to_have_a_last_name()
    {
        try {
            $project = factory(Project::class)->create([
                'last_name' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a project without a last name, but an exception was not thrown.');
    }
    
    /**
     * @test
     * Creating a Project without an email
     *
     * @return void
     */
    public function a_project_has_to_have_a_email()
    {
        try {
            $project = factory(Project::class)->create([
                'email' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a project without an email, but an exception was not thrown.');
    }

    /**
     * @test
     * A project should have an address
     *
     * @return void
     */
    public function a_project_should_have_an_address()
    {
        try {
            $project = factory(Project::class)->create([
                'address_id' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a project without an address, but an exception was not thrown.');
    }
}
