<?php

namespace Tests\Unit;

use App\Models\Company;
use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     * A User can be created
     *
     * @return void
     */
    public function it_can_create_a_user()
    {
        $data = [
            'first_name' => 'Test',
            'last_name' => 'User',
            'email' => 'email@email.com',
            'password' => 'asdadasdsad',
        ];

        $user = new User($data);
        $user->save();
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($data['first_name'], $user->first_name);
        $this->assertEquals($data['last_name'], $user->last_name);
        $this->assertEquals($data['email'], $user->email);
        $this->assertEquals($data['password'], $user->password);
    }
}
