<?php

namespace Tests\Unit;

use App\Models\Company;
use App\Models\Form;
use Tests\TestCase;

class FormTest extends TestCase
{
    /**
     * @test
     * A Form can be created
     *
     * @return void
     */
    public function it_can_create_a_form()
    {
        $company = factory(Company::class)->create();
        $data = [
            'name' => 'Test Form',
        ];

        $form = new Form($data);
        $company->forms()->save($form);
        $this->assertInstanceOf(Form::class, $form);
        $this->assertEquals($data['name'], $form->name);
    }

     /**
     * @test
     * A Form should have a name
     *
     * @return void
     */
    public function a_form_should_have_a_name()
    {
        try {
            $form = factory(Form::class)->create([
                'name' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }
        $this->fail('We tried to create a form without a name, but an exception was not thrown.');
    }

     /**
     * @test
     * Creating a Form without a Company
     *
     * @return void
     */
    public function a_form_has_to_have_a_company_id()
    {
        try {
            $form = factory(Form::class)->create([
                'company_id' => null,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('We tried to create a form without a company, but an exception was not thrown.');
    }
}

