<?php

namespace Tests\Traits;

use App\Models\AdminUser;

// use Laravel\Dusk\Browser;

trait TestAdminUserSetupTrait
{
    protected $user = null;
    private $browser = null;
    private $plain_password = null;

    public function createAdminUser($password)
    {
        $this->plain_password = $password;

        $this->user = factory(AdminUser::class)->create([
            'password' => bcrypt($password),
        ]);
    }

    protected function doLogin($app_url)
    {
        // $this->browse(function (Browser $browser) use ($app_url) {
        //     $browser->visit($app_url . '/login')
        //         ->assertSee('E-Mail Address')
        //         ->type('email', $this->user->email)
        //         ->type('password', $this->plain_password)
        //         ->press('Sign In');

        //     $this->browser = $browser;
        // });
    }
}
