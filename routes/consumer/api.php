<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Consumer\Api\V1'], function () {
    Route::post('login', 'LoginController@login');

    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('profile', 'ProfileController@index');
        Route::post('login/refresh', 'LoginController@refresh');
        Route::post('logout', 'LoginController@logout')->name('logout');
    });
});
