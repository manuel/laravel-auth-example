<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Route::get('/check', 'HealthCheckController@check');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'Consumer\DashboardController@index');
});

Route::get('login', 'Consumer\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Consumer\Auth\LoginController@login');

Route::get('register', 'Consumer\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Consumer\Auth\RegisterController@register');


Route::get('password/reset', 'Consumer\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Consumer\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Consumer\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Consumer\Auth\ResetPasswordController@reset')->name('password.update');


Route::get('email/verify', 'Consumer\Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Consumer\Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Consumer\Auth\VerificationController@resend')->name('verification.resend');
