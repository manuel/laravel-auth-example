@extends('layouts.marketing')

@section('content')
    <div class="border-b border-grey-light p-4 px-3 py-10 bg-grey-lighter flex justify-center min-h-full"><div class="w-full max-w-sm">
            <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="mb-4">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <div class="mb-4">
                    <label for="email" class="block text-grey-darker text-sm font-bold mb-2">Reset Password</label>
                    <input id="email" type="email" placeholder="E-Mail Address" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <p class="text-red text-xs italic">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="flex items-center justify-between">
                    <button type="submit" class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded">Send Password Reset Link</button>
                    <a href="{{ route('login') }}" class="inline-block align-baseline px-4 text-sm text-blue hover:text-blue-darker">Login</a>
                </div>
            </form>
            <p class="text-center text-grey text-xs">©2018 {{ config('app.name') }}. All rights reserved.</p>
        </div>
    </div>
@endsection
