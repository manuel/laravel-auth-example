@extends('layouts.marketing')

@section('content')
<div class="w-full flex flex-col items-center">
    <div class="max-w-xs mt-10">
          <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('admin.login') }}">
            @csrf
            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                {{ __('E-Mail Address') }}
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="email" name="email" type="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                     <p class="text-red text-xs italic" role="alert">
                        {{ $errors->first('email') }}</strong>
                    </p>
                @endif
            </div>
            <div class="mb-6">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                {{ __('Password') }}
              </label>
              <input class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="" name="password" required>
                @if ($errors->has('password'))
                    <p class="text-red text-xs italic" role="alert">
                        {{ $errors->first('password') }}
                    </p>
                @endif
            </div>
            <div class="flex items-center justify-between">
              <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                {{ __('Login') }}
              </button>
              <a class="ml-4 inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker" href="{{ route('admin.password.request') }}">
                {{ __('Forgot Your Password?') }}
              </a>
            </div>
          </form>
          <p class="text-center text-grey text-xs">
            ©2018 {{ config('app.name') }}. All rights reserved.
          </p>
    </div>
</div>
 @endsection
