<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="icon.png">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @if($app->environment('local') || $app->environment('development'))
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    @else
    <link href="{!! mix('css/admin.css') !!}" rel="stylesheet">
    @endif
</head>
<body lang="{{ app()->getLocale() }}">
    @yield('content')
    <!-- Scripts -->
    @if($app->environment('local') || $app->environment('development'))
        <script src="/js/admin.js"></script>
    @else
        <script src="{!! mix('js/admin.js') !!}"></script>
    @endif
</body>
</html>
