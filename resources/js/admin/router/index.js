import Vue from 'vue';
import Router from 'vue-router';

import RootContainer from '../containers/root-container.vue';

import Dashboard from '../views/dashboard.vue';


// Views
Vue.use(Router);

export default new Router({
    linkActiveClass: 'border-blue-dark',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/',
            component: RootContainer,
            props: { isFullScreen: true },
            children: [
                {
                    path: '/',
                    component: Dashboard,
                },
            ],
        },
    ],
});
