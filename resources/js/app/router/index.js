import Vue from 'vue';
import Router from 'vue-router';

// Layout
import ChildLayout from '../views/layout/child.vue';

// Views
import Home from '../views/home.vue';

Vue.use(Router);

export default new Router({
    linkActiveClass: 'border-dark',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/',
            component: Home,
        },
    ],
});
