// initial state
const state = {
    profile: {}
};

// getters
const getters = {
};

// actions
const actions = {

};

// mutations
const mutations = {
    setProfile(state, profile) {
        state.profile = profile;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
