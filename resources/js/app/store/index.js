import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';
import user from './modules/user';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({

    modules: {
        user,
    },
    state: {
    },
    strict: debug,
    mutations: {
    }
});
